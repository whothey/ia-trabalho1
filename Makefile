# General config
CC=g++
LIBS=-lGLU -lGL -lglut -lm
FLAGS=-Wall
BINDIR=bin

########################################################
# Options
########################################################

# Reward mapping: simple is without the distance sensor
REWARDS=REWARDS_COMPLEX     # REWARDS_SIMPLE or REWARDS_COMPLEX

SUCCESS_REWARD=1000.0
EXPLOSION_REWARD=0.1
# Initial reward
DEFAULT_REWARD=1.0

# Frequency of glutTimerFunction in ms
UPDATE_FREQ=0.1             # float

# Number of generated maps
NTESTS=500            # int unsigned

# When PRACTICE is ON (1), random actions will be taken, without evaluating the rewards of the next
# action, but the values still will be updated
PRACTICE=ACTION_DETERM                  # ACTION_BIASED, ACTION_RANDOM, ACTION_DETERM, ACTION_DIAG

# Value file to store or update states-values
VALUES="values.csv"
VALUE_FILE="\"$(VALUES)\""

# Temporal Difference Setup
#
# The Temporal Difference Learning Rate
# float
INITIAL_LEARNING_RATE=0.1
# Temporal Diffenrence "next state" value extraction
VALUE_EXTRACTION=0.98      # float
# Max moves to be stored in backpropagation queue
MAX_MOVES=200              # float

# Map Level
DIFICULTY=DIFICULTLEVELEASY # DIFICULTLEVELEASY, DIFICULTLEVELAVG or DIFICULTLEVELDIF

########################################################
# Parse options
########################################################
MACROS=                               \
	   -DREWARDS=$(REWARDS)\
	   -DUPDATE_FREQ=$(UPDATE_FREQ)\
	   -DDIFICULTY=$(DIFICULTY)\
	   -DDEFAULT_REWARD=$(DEFAULT_REWARD)\
	   -DINITIAL_LEARNING_RATE=$(INITIAL_LEARNING_RATE)\
	   -DPRACTICE=$(PRACTICE)\
	   -DMAX_MOVES=$(MAX_MOVES)\
	   -DVALUE_FILE=$(VALUE_FILE)\
	   -DSUCCESS_REWARD=$(SUCCESS_REWARD)\
	   -DEXPLOSION_REWARD=$(EXPLOSION_REWARD)\
	   -DNTESTS=$(NTESTS)

all: main

practice_diag:
	$(eval PRACTICE=ACTION_DIAG)

practice_run:
	$(eval PRACTICE=ACTION_DETERM)

practice_biased:
	$(eval PRACTICE=ACTION_BIASED)

n1:
	$(eval NTESTS=1)

n50:
	$(eval NTESTS=50)

n100:
	$(eval NTESTS=100)

n1000:
	$(eval NTESTS=1000)

diag: practice_diag main exec

biased: practice_biased main exec

run: practice_run main exec

exec: bin/main
	bin/main

main: main.cpp orunner.o
	$(CC) $< "$(BINDIR)/orunner.o" -o "$(BINDIR)/$@" $(MACROS) $(LIBS) $(FLAGS)

orunner.o: ObstacleRunner.cpp
	$(CC) -c $< -o "$(BINDIR)/$@" $(FLAGS)

clean:
	rm ./bin/main ./bin/orunner.o

clear:
	rm ./$(VALUES)

cleanup: clean clear

view_values:
	cat <<< "D,SL,SF,SE,SO,VAL" | cat - $(VALUES) | column -t -s, | less -

view_svalues:
	cat <<< "D,SL,SF,SE,SO,VAL" | cat - $(VALUES) | sort -t, -n -k6 | column -t -s,  | less -

view_rvalues:
	cat <<< "D,SL,SF,SE,SO,VAL" | cat - $(VALUES) | sort -r -t, -n -k6 | column -t -s, | less -

.PHONY: view_values view_svalues view_rvalues
