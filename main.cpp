#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <time.h>
#include "ObstacleRunner.h"

map *corredor=NULL;

// Rewards type
#define REWARDS_SIMPLE  1
#define REWARDS_COMPLEX 2

#define ACTION_BIASED 0
#define ACTION_RANDOM 1
#define ACTION_DETERM 2
#define ACTION_DIAG   3

#ifndef PRACTICE
 #define PRACTICE 1
#endif

#ifndef SUCCESS_REWARD
 #define SUCCESS_REWARD 100.0
#endif

#ifndef EXPLOSION_REWARD
 #define EXPLOSION_REWARD -100.0
#endif

#ifndef DEFAULT_REWARD
 #define DEFAULT_REWARD 0.0
#endif

#ifndef VALUE_FILE
 #define VALUE_FILE "values.csv"
#endif

#ifndef INITIAL_LEARNING_RATE
 #define INITIAL_LEARNING_RATE 0.01
#endif

#if PRACTICE == ACTION_DETERM
 #define LEARNING_RATE(qtd) (INITIAL_LEARNING_RATE * (float(NTESTS - (qtd) + 1) / NTESTS))
#else
 #define LEARNING_RATE(qtd) (INITIAL_LEARNING_RATE)
#endif

#ifndef MAX_MOVES
 #define MAX_MOVES 100
#endif

#ifndef VALUE_EXTRACTION
 #define VALUE_EXTRACTION 0.98
#endif

#ifndef DIFICULTY
 #define DIFICULTY DIFICULTLEVELEASY
#endif

#ifndef REWARDS
 #define REWARDS REWARDS_COMPLEX
#endif

#ifndef UPDATE_FREQ
 #define UPDATE_FREQ 0.1
#endif

#ifndef NTESTS
 #define NTESTS 100
#endif

#if REWARDS == REWARDS_SIMPLE
// Rewards Mapping
//            |------------------------ State Mapping -----------------------|
//            |                                                              |
//            8 Directions      Sensor Left      Sensor Front     Sensor Right
float values[NUMBERDIRECTIONS][NUMBERDISTANCES][NUMBERDISTANCES][NUMBERDISTANCES]
#else
// Rewards Mapping
//            |---------------------------------- State Mapping ---------------------------------|
//            |                                                                                  |
//            8 Directions      Sensor Left      Sensor Front     Sensor Right     Sensor Distance
float values[NUMBERDIRECTIONS][NUMBERDISTANCES][NUMBERDISTANCES][NUMBERDISTANCES][NUMBERDISTANCES];
#endif

state moves[MAX_MOVES];

state st;
action ac;
int maps=0;
int acs=0;
int boom=0;
int suc=0;
int exausted=0;
int last_direction=5;

void print_state(state ps)
{
    int
        fs = ps.frontDistance(),
        rs = ps.rightDistance(),
        ls = ps.leftDistance(),
        ts = ps.targetDistance(),
        dr = ps.getDir();

    printf("%d %d %d %d %d", dr, ls, fs, rs, ts);
}

float state_value(state st)
{
    int
        fs = st.frontDistance(),
        rs = st.rightDistance(),
        ls = st.leftDistance(),
        ts = st.targetDistance(),
        dr = st.getDir();

    return values[dr][ls][fs][rs][ts];
}

void set_state_value(state st, float value) {
    int
        fs = st.frontDistance(),
        rs = st.rightDistance(),
        ls = st.leftDistance(),
        ts = st.targetDistance(),
        dr = st.getDir();

    values[dr][ls][fs][rs][ts] = value;
}

void set_state_explosion(state st)
{
    printf("Set explosion on ");
    print_state(st);
    printf("\n");

    set_state_value(st, EXPLOSION_REWARD);
}

void set_state_success(state st)
{
    set_state_value(st, SUCCESS_REWARD);
}

float state_reward(state st)
{
    int
        fd = st.frontDistance(),
        ld = st.leftDistance(),
        rd = st.rightDistance(),
        td = st.targetDistance();
    
    // OLD
    //float
    //    reward_front = 1.50 * EXPLOSION_REWARD / (NUMBERDISTANCES - fd),
    //    reward_left  = 1.25 * EXPLOSION_REWARD / (NUMBERDISTANCES - ld),
    //    reward_right = 1.25 * EXPLOSION_REWARD / (NUMBERDISTANCES - rd),
    //    reward_obj   = 0.50 * SUCCESS_REWARD / (td + 1);

    float
        reward_front = (0.125 * 0.2500 * SUCCESS_REWARD) / (NUMBERDISTANCES - fd),
        reward_left  = (0.250 * 0.0625 * SUCCESS_REWARD) / (NUMBERDISTANCES - ld),
        reward_right = (0.250 * 0.0625 * SUCCESS_REWARD) / (NUMBERDISTANCES - rd),
        reward_obj   = (0.500 * SUCCESS_REWARD) / (td + 1);

    // Test
    //float
    //    reward_front = EXPLOSION_REWARD * 2 * fd,
    //    reward_left  = EXPLOSION_REWARD * ld,
    //    reward_right = EXPLOSION_REWARD * rd,
    //    reward_obj   = (0.8 * SUCCESS_REWARD/100) / (td + 1);

    return reward_front + reward_left + reward_right + reward_obj;
}

action randomAction(state st) {
    int d = randomIntValue(0, NUMBERDIRECTIONS - 1);
    int v = randomIntValue(0, NUMBERDISTANCES - 1);

    ac.setAction(d,v);

    return ac;
}

action biasedAction(state st) {
    int d = ac.getPolicyDirection();
    int v = ac.getPolicyVelocity();

    ac.setAction(d,v);

    return ac;
}

action chooseAction(state st) {
    action test_action, candidates[NUMBERDIRECTIONS * RUNNERVELOCITIES], chosen;
    state  next, best;
    int    i, j, candidates_count, chosen_index;

    test_action.setAction(last_direction, 0);
    next = best = corredor->getNextState(test_action);
    candidates_count = 0;

    //printf("@ "); print_state(st); printf(":\n");

   for (i = 0; i < NUMBERDIRECTIONS; i++) {
       for (j = 0; j < RUNNERVELOCITIES; j++) {
           test_action.setAction(i, j);
           next = corredor->getNextState(test_action);

           //printf("Candidate (%d, %d) [ ", test_action.getDirection(), test_action.getVelocity());
           //print_state(next);
           //printf(" ] %f\n", state_value(next));

           //printf("Eval: (%d,%d) => %f | Best: (%d,%d) => %f\n", i,j, state_value(next), chosen.getDirection(), chosen.getVelocity(), state_value(best));

           //if (state_reward(best) < state_reward(next)) {
           if (state_value(best) < state_value(next)) {
               best = next;
               candidates[0] = test_action;
               candidates_count = 1;
           } else if (state_value(best) == state_value(next)) {
               candidates[candidates_count] = test_action;
               candidates_count++;
           }
       }
   }

   chosen_index = randomIntValue(0, candidates_count - 1);
   //printf("Candidates: %d | Chosen: %d\n", candidates_count, chosen_index);
   //for (i = 0; i < candidates_count; i++)
   //    printf("Candidate %d: (%d, %d) %f\n", i, candidates[i].getDirection(), candidates[i].getVelocity(), state_value(corredor->getNextState(candidates[i])));

   chosen = candidates[chosen_index];

   if (last_direction != chosen.getDirection())
       last_direction = chosen.getDirection();

   //printf("Chosen: (%d,%d) => %f\n", chosen.getDirection(), chosen.getVelocity(), state_value(best));
   //best.print();
   //chosen.print();

   return chosen;
}

void temporal_dif(state *moves, int n, float final_reward)
{
    int i = 0;
    float sv = 0, snv = 0;

    printf("N Temporal Dif: %d to %f\n", n, final_reward);

    //n--;
    //sv  = state_value(moves[n]);
    //snv = final_reward;
    //set_state_value(moves[n], sv + LEARNING_RATE(maps) * (snv + VALUE_EXTRACTION * snv - sv));
    //set_state_value(moves[n], state_value(moves[n]) + final_reward);
    //set_state_value(moves[n], final_reward);

    /**
     * n-1 is the 'current state'
     * n is the next one
     */
    for (i = 0; i < n; i++) {
        sv  = state_value(moves[i]);
        snv = state_value(moves[i+1]);
        //set_state_value(moves[n-1], sv + LEARNING_RATE(maps) * (state_reward(moves[n-1]) + VALUE_EXTRACTION * snv - sv));
        set_state_value(moves[i], sv + LEARNING_RATE(maps) * (state_reward(moves[i+1]) + VALUE_EXTRACTION * snv - sv));
        //set_state_value(moves[n-1], state_reward(moves[n-1]));
    }

    sv  = state_value(moves[i]);
    snv = final_reward;
    set_state_value(moves[i], sv + LEARNING_RATE(maps) * (final_reward + VALUE_EXTRACTION * snv - sv));
}

void init_rewards() {
    state s;
#if REWARDS == REWARDS_COMPLEX
    int i, j, k, l, m;
#else
    int i, j, k, l;
#endif

   for (i = 0; i < NUMBERDIRECTIONS; i++)
       for (j = 0; j < NUMBERDISTANCES; j++)
           for (k = 0; k < NUMBERDISTANCES; k++)
               for (l = 0; l < NUMBERDISTANCES; l++)
#if REWARDS == REWARDS_COMPLEX
                   for (m = 0; m < NUMBERDISTANCES; m++) {
                       s.setState(i, j, k, l, m);
                       values[i][j][k][l][m] = state_reward(s);
                   }
#else
                   values[i][j][k][l] = DEFAULT_REWARD;
#endif
}

void dump_rewards(FILE *f) {
#if REWARDS == REWARDS_COMPLEX
    int i, j, k, l, m;
#else
    int i, j, k, l;
#endif

    for (i = 0; i < NUMBERDIRECTIONS; i++)
        for (j = 0; j < NUMBERDISTANCES; j++)
            for (k = 0; k < NUMBERDISTANCES; k++)
                for (l = 0; l < NUMBERDISTANCES; l++)
#if REWARDS == REWARDS_COMPLEX
                    for (m = 0; m < NUMBERDISTANCES; m++)
#endif
#if REWARDS == REWARDS_COMPLEX
                                fprintf(f, "%d,%d,%d,%d,%d,%f\n", i,j,k,l,m, values[i][j][k][l][m]);
#else
                                fprintf(f, "%d,%d,%d,%d,%d,%d,%f\n", i,j,k,l, rewards[i][j][k][l]);
#endif
}

void load_rewards(FILE *f) {
#if REWARDS == REWARDS_COMPLEX
    int i, j, k, l, m;
#else
    int i, j, k, l;
#endif

    do {
#if REWARDS == REWARDS_COMPLEX
        fscanf(f, "%d,%d,%d,%d,%d,", &i, &j, &k, &l, &m);
        fscanf(f, "%f\n", &values[i][j][k][l][m]);
#else
        fscanf(f, "%d,%d,%d,%d,", &i, &j, &k, &l);
        fscanf(f, "%f\n", &rewards[i][j][k][l]);
#endif
    } while(! feof(f));
}

void process(int){
    FILE *dump_file;
  st = corredor->getState();
  //st.print();
  //printf("\tReward=%f\n\tValue=%f\n", state_reward(st), state_value(st));

#if PRACTICE == ACTION_RANDOM
  ac = randomAction(st);
#elif PRACTICE == ACTION_BIASED
  ac = biasedAction(st);
#elif PRACTICE == ACTION_DIAG
  ac.setAction(1, randomIntValue(0, 2));
#else
  ac = chooseAction(st);
#endif

  corredor->move(ac);

  set_state_value(st, state_value(st) + LEARNING_RATE(maps) * (state_reward(st) + VALUE_EXTRACTION * state_value(corredor->getState()) - state_value(st)));

  moves[acs++] = corredor->getState();

  //moves[acs].print();
  //ac.print();

  /**
   * Quando o Corredor alcança o seu objetivo ou bate em alguma parede
   * o seu "currentState" não é atualizado, sendo assim, para pegar o
   * real estado final é preciso simular a ação com o "getNextState()"
   */
  if (corredor->succeeded()) {
    suc++;
    //set_state_success(moves[acs-1]);
      // Propagate back the values
    set_state_value(st, state_value(st) + LEARNING_RATE(maps) * (state_reward(st) + VALUE_EXTRACTION * SUCCESS_REWARD - state_value(st)));
    //temporal_dif(moves, acs, SUCCESS_REWARD);
    printf("Sucesso em %d actions\n", acs);
  }

  if (corredor->exploded()) {
    boom++;
    //set_state_explosion(moves[acs-1]);
    //temporal_dif(moves, acs, EXPLOSION_REWARD);
    set_state_value(st, state_value(st) + LEARNING_RATE(maps) * (state_reward(st) + VALUE_EXTRACTION * EXPLOSION_REWARD - state_value(st)));
    printf("Explodiu em %d actions\n", acs);
  }

  if (acs >= MAX_MOVES) {
      exausted++;
      //set_state_explosion(corredor->getNextState(ac));
      //temporal_dif(moves, acs, state_value(moves[acs-1]));
      printf("Exausto em %d actions\n", acs);
  }

  desenha();

  if (corredor->succeeded() || corredor->exploded() || acs >= MAX_MOVES) {
    delete corredor;
    acs=0;

    printf("\tSucessos=%d\tExplosoes=%d\tExausto=%d\t(Aproveitamento: %.2f%%)\n",suc,boom,exausted, float(suc)/(boom + exausted) * 100);
    printf("\tTaxa de Aprendizado: %f\n", LEARNING_RATE(maps));
    if (maps >= NTESTS) {
      printf("Resumo:\n\tSucessos=%d\n\tExplosoes=%d\n\tExausto=%d\n",suc,boom,exausted);
      dump_file = fopen("./" VALUE_FILE, "w+");
      dump_rewards(dump_file);
      fclose(dump_file);
      exit(EXIT_SUCCESS);
    }
    corredor = new map(DIFICULTY);
    corredor->setCurrentMap();
    maps++;
    printf("Mapa %d: ", maps);
  }

  // Estes comandos devem sempre terminar a função
  // Pode trocar o valor do tempo se quizer que programa
  // rode mais rápido ou lentamente
  glutPostRedisplay();
  glutTimerFunc(UPDATE_FREQ, process, 1);
}

int main(int argc, char **argv)
{
    FILE *rws;
    srand (time(NULL));

    printf("Rewards:\n\tSUCCESS=%f\n\tEXPLOSION=%f\n\tDEFAULT=%f\n", SUCCESS_REWARD, EXPLOSION_REWARD, DEFAULT_REWARD);

    // Inicializa o array de recompensas
    init_rewards();

    if ((rws =  fopen("./" VALUE_FILE, "r"))) {
        load_rewards(rws);
        fclose(rws);
    }

    corredor = new map(DIFICULTY);
    corredor->setCurrentMap();
    maps = 1;
    corredor->initializeMap(argc, argv);

    return 0;
}
