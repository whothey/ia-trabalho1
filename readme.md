# Aprendizagem Por Reforço

Alunos: Gabriel Henrique Rudey, Ricardo Augusto Müller

## Parametrização

Para alterar a parametrização do programa é possível editar o arquivo
`Makefile`, há várias variaveis documentadas para alteração do software,
com valores padrões no código caso estas não sejam definidas.

Essas variaveis só são atribuídas caso o programa seja compilado pelo
utilitário Make, com instruções disponíveis posteriormente.

## Tipos de Decisão de Ações

Para definir a decisão da próxima ação, altere a variável `PRACTICE` do
`Makefile` com um dos seguintes valores:

* `ACTION_DIAG`: O jogador sempre decide pela direção NE com velocidades
variando (0 à 2)
* `ACTION_BIASED`: Jogador tem uma tendência maior de selecionar as direções
N, NE, E
* `ACTION_DETERM`: O jogador sempre decide pela ação que levará para
um estado com maior valor, caso mais de uma ação leve à estados de
mesmo valor, um destes será sorteado

Mais à diante há instruções de compilação com estas flags específicas.

## Executando o programa

### Compilando

Para compilar o programa é só fazer uso do utilitário Make

```sh
make
```
Para executar o programa faça:

```sh
make run
```

### Executando Testes

Há tarefas definidas no utilitário Make para facilitar e agilizar a
recompilação e testes.

Como a parametrização é frequentemente alterada para alternar entre 
testes e execução com as decisões dos valores dos estados, para isso
é possível utilizar três comandos:

```sh
make diag   # O jogador sempre toma NE como decisão de movimento
make biased # O jogador tem uma probabilidade maior de seguir as direções N, NE, E
make run    # O jogador toma as decisões com base nos valores dos estados
```

Nos três comandos anteriores o programa é recompilado com as devidas
flags e executado em seguida.

### Verificando Valores

Para verificar os valores dos estados conseguidos após um teste
utilize os comandos:

ATENÇÃO: Estes comandos somente funcionam para um sistema baseado
em Unix com os utilitários Make, Less, GNUtils (sort e column).

```sh
make view_values  # Inicia leitura pelo comando LESS dos estados com seus valores
make view_svalues # Inicia leitura pelo comando LESS dos estados com seus valores ordenados
make view_rvalues # Inicia leitura pelo comando LESS dos estados com seus valores ordenados inversamente
```

### Limpando

Para limpar os arquivos compilados ou o arquivo de valores de
estados utilize os comandos:

```sh
make clean   # Limpa os arquivos compilados, apenas
make clear   # Limpa o arquivo de valores dos estados
make cleanup # Limpa os arquivos compilados e de valores de estados
```
